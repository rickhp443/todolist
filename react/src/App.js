import React, { Component } from 'react';
import './App.css';
import Todo from './Todo.js'
import {Editing,Processing} from './Util.js'
class App extends Component{
  constructor(props){
    super(props);
    this.state={
      todos:[],
      todoText: '',
      editingTodo:{},
      isEditing:false,
      isProcessing:false
    };
    this.id=1;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.addTodo = this.addTodo.bind(this);
    this.deleteTodo = this.deleteTodo.bind(this);

    this.editTodo = this.editTodo.bind(this);
    this.handleEditingChange = this.handleEditingChange.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.cancelEdit = this.cancelEdit.bind(this);
  }
  
  handleSubmit(e){
    this.setState({
      todoText:e.target.value,
    });
  }
  addTodo(){
    const { todoText,todos } = this.state;
    this.setState({
      todos:[...todos,{
        id:this.id,
        isCompleted:false,
        text:todoText
      }],
      todoText:"",
    })
    this.id++;
  }
  deleteTodo(id){
    this.setState({
      todos:this.state.todos.filter(todo => todo.id !== id)
    });
  }

  editTodo = (id, content) => {
    this.setState({
     editingTodo: { id: id, content: content },
     isEditing: true,
     isProcessing: true
    })
  }
  handleEditingChange = (text) => {
    // 從傳入的改變，id 直接取暫存區的即可。
    const { editingTodo } = this.state
    this.setState({
      editingTodo: {
        ...editingTodo,
        text: text
      }
    })
  }
  handleUpdate = (e) => {
    const { editingTodo, todos } = this.state
    this.setState({
      todos: todos.map(todo => {
        if (e === editingTodo.id && editingTodo.id === todo.id) return {
          ...todo,
          text: editingTodo.text
        }
        return todo;
      }),
      isEditing: false,
      isProcessing: false,
      editingTodo: {},
    })
  }
  cancelEdit = () => {
    this.setState({
      isEditing: false,
      isProcessing: false
    })
  }
  render() {
    const { todos, todoText, isEditing, isProcessing, editingTodo } = this.state
    return (
      <div>
        <div className="todolist__input">
          <h1> todolist </h1>
          <input className="form-control form-control-sm" type="text" name="todo" onChange={this.handleSubmit} />
          <button type="button" className="todolist__edit btn btn-outline-secondary" onClick={this.addTodo}>ADD</button>
          <hr />
        </div>
      
        <div className="todolist__list">
          {todos.map(todo => (<Todo todo={todo} key={todo.id} deleteTodo={this.deleteTodo} editTodo={this.editTodo}/>))}
        </div>
        {isEditing && <Editing cancelEdit={this.cancelEdit}
         editingTodo={editingTodo} 
         handleEditingChange={this.handleEditingChange} 
         handleUpdate={this.handleUpdate} />}
      </div>
    );
  }
}

export default App;

//placeholder="輸入代辦事項輸入完成按下 enter 送出" onKeyPress
/*handleSubmit = (e) => {
    let todoText=e.target.value;
    //const { todos, todoText } = this.state
    if (e.key === 'Enter') {
      this.setState({
        todos: [{
          id: this.identify,
          isCompleted: false,
          content: todoText,
        }],
        todoText: '',
      },()=>console.log(this.state));
      this.identify++;
    }
  }*/