import React, { Component } from 'react';

class Todo extends Component{
    constructor(props){
        super(props);
        this.delete=this.delete.bind(this);
        this.edit = this.edit.bind(this);
    }
    delete(){
        const {todo,deleteTodo} = this.props;
        deleteTodo(todo.id);
    }
    edit(){
        const { editTodo, todo } = this.props;
        editTodo(todo.id, todo.content);
    }
    render(){
        const {todo} = this.props;
        return(
            <div className="todotlist__item form-check">
                <input className="todolist__done form-check-input" type="checkbox" name="done" />
                <label className="todolist__content form-check-label">
                    {todo.text}
                </label>
                <button type="button" className="todolist__edit btn btn-outline-secondary" onClick={this.edit}>編輯</button>
                <button type="button" className="todolist__delete btn btn-outline-secondary" onClick={this.delete}>刪除</button>
                <hr />
            </div>
        );
    }
}

export default Todo


//https://hugh-program-learning-diary-js.medium.com/%E5%89%8D%E7%AB%AF%E6%A1%86%E6%9E%B6-react-todolist-3ef24b08a444

//https://hugh-program-learning-diary-js.medium.com/%E5%89%8D%E7%AB%AF%E6%A1%86%E6%9E%B6-react-hw1-todolist-%E6%94%B9%E5%AF%AB-b02f52e9a334